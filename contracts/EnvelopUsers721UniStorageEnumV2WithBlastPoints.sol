// SPDX-License-Identifier: MIT
// ENVELOP protocol for NFT. Mintable User NFT Collection
pragma solidity 0.8.21;

import "./EnvelopUsers721UniStorageEnumV2.sol";
import "./BlastPoints.sol";


contract EnvelopUsers721UniStorageEnumV2WithBlastPoints is EnvelopUsers721UniStorageEnumV2, BlastPoints {
    

    constructor(
        string memory name_,
        string memory symbol_,
        string memory _baseurl,
        address _pointsOperator
    ) 
        EnvelopUsers721UniStorageEnumV2(name_, symbol_, _baseurl)
        BlastPoints(_pointsOperator)
    {
    }

    
}