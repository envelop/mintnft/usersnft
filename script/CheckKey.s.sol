// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {EnvelopUsers1155UniStorageV2} from "../contracts/EnvelopUsers1155UniStorageV2.sol";
import {EnvelopUsers721UniStorageEnumV2} from "../contracts/EnvelopUsers721UniStorageEnumV2.sol";

contract CheckKey is Script {
    using stdJson for string;

    function run() public {
        console2.log("Deployer address: %s", vm.addr(vm.envUint("DEV_PRIVATE_KEY")));
        //console2.log("ETH_KEYSTORE_ACCOUNT: %s", vm.envString("ETH_KEYSTORE_ACCOUNT")); 
        //console2.log("ETH_KEYSTORE : %s", vm.envString("ETH_KEYSTORE")); 
        //address senderad = 0xDDA2F2E159d2Ce413Bd0e1dF5988Ee7A803432E3;
        vm.startBroadcast();
        console2.log(msg.sender);
        address payable geteth = payable(address(this));
        geteth.transfer(1);
        // EnvelopUsers721UniStorageEnumV2 mint721 = new EnvelopUsers721UniStorageEnumV2(
        //     "Envelop Users NFT 721", 
        //     "ENVELOP", 
        //     "https://api.envelop.is/metadata/"
        // );
        vm.stopBroadcast();
        
        

    }
}
