// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";

contract CheckKeyExample is Script {
    using stdJson for string;

    function run() public {
        vm.startBroadcast();
        console2.log(msg.sender);
        //address payable geteth = payable(address(this));
        address payable geteth = payable(msg.sender);
        geteth.transfer(1);
        vm.stopBroadcast();
    }
}
