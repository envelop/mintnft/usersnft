// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {EnvelopUsers1155UniStorageV2WithBlastPoints} from "../contracts/EnvelopUsers1155UniStorageV2WithBlastPoints.sol";
import {EnvelopUsers721UniStorageEnumV2WithBlastPoints} from "../contracts/EnvelopUsers721UniStorageEnumV2WithBlastPoints.sol";

contract DeployScriptBlast is Script {
    using stdJson for string;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Deployer address: %s, %s", msg.sender, msg.sender.balance);
        
        address pointsOperator = 0x303cD2A927D9Cb6F5CE03b88a4e3E2528baEDF40;
        vm.startBroadcast();
        EnvelopUsers721UniStorageEnumV2WithBlastPoints mint721 = new EnvelopUsers721UniStorageEnumV2WithBlastPoints(
            "Envelop Users NFT 721", 
            "ENVELOP", 
            "https://api.envelop.is/metadata/",
            pointsOperator
        );
        EnvelopUsers1155UniStorageV2WithBlastPoints mint1155 = new EnvelopUsers1155UniStorageV2WithBlastPoints(
            "Envelop Users NFT 1155", 
            "ENVELOP", 
            "https://api.envelop.is/metadata/",
            pointsOperator
        );
        vm.stopBroadcast();
        
        ///////// Pretty printing ////////////////
        
        string memory root = vm.projectRoot();
        string memory path = string.concat(root, "/script/explorers.json");
        string memory json = vm.readFile(path);
        console2.log("Chain id: %s", vm.toString(block.chainid));
        string memory explorer_url = json.readString(
            string.concat(".", vm.toString(block.chainid))
        );
        
        console2.log("**EnvelopUsers721UniStorageEnumV2WithBlastPoints**");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(mint721));
        console2.log("**EnvelopUsers1155UniStorageV2WithBlastPoints**");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(mint1155));

        console2.log("```python");
        console2.log("mint721 = EnvelopUsers721UniStorageEnumV2WithBlastPoints.at('%s')", address(mint721));
        console2.log("mint1155 = EnvelopUsers1155UniStorageV2WithBlastPoints.at('%s')", address(mint1155));
        console2.log("```");

        ///////// End of pretty printing ////////////////

        //console2.log("mint721.owner(): %s", mint721.owner());
        ///  Init ///
        vm.startBroadcast();
        mint721.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/');
        mint721.setPrefixURI('ipfs', 'ipfs://');

        mint1155.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/');
        mint1155.setPrefixURI('ipfs', 'ipfs://');
        vm.stopBroadcast();

    }
}
