import pytest
import logging
from brownie import Wei, reverts, chain, web3
from web3 import Web3
from eth_abi import encode_single
from eth_account.messages import encode_defunct


LOGGER = logging.getLogger(__name__)

ORACLE_ADDRESS = '0x8125F522a712F4aD849E6c7312ba8263bEBeEFeD' 
ORACLE_PRIVATE_KEY = '0x222ead82a51f24a79887aae17052718249295530f8153c73bf1f257a9ca664af'
zero_address = '0x0000000000000000000000000000000000000000'

def test_mint(accounts, NFTMinter1155Uni):
    
    tokenId = 1
    tokenUri = 'b72f05424ee87a65cb7c94b432d3b5b553bbb82f7b0fe34e8a3ad161b1b05ca5/'
    amount = 3
    #Message for sign
    encoded_msg = encode_single(
         '(address,uint256,string,uint256)',
         ( accounts[0].address, 
           Web3.toInt(tokenId) ,
           tokenUri,
           Web3.toInt(amount)
         )
    )

    encoded_msg_wrong = encode_single(
         '(address,uint256,string,uint256)',
         ( accounts[0].address, 
           Web3.toInt(tokenId+1) ,
           tokenUri,
           Web3.toInt(amount)
         )
    )

    hashed_msg = Web3.solidityKeccak(['bytes32'], [encoded_msg])
    hashed_msg_wrong = Web3.solidityKeccak(['bytes32'], [encoded_msg_wrong])
    ## Block Above has too complicated param for encode in web3py
    ## So  use helper from contract
    
    logging.info('hashed_msg = {}'.format(hashed_msg))
    # Ether style signature
    message = encode_defunct(primitive=hashed_msg)
    message_wrong = encode_defunct(primitive=hashed_msg_wrong)

    logging.info('message = {}'.format(message))
    signed_message = web3.eth.account.sign_message(message, private_key=ORACLE_PRIVATE_KEY)
    signed_message_wrong = web3.eth.account.sign_message(message_wrong, private_key=ORACLE_PRIVATE_KEY)

    logging.info('sign_message is {}'.format(signed_message))
    logging.info('sign_message is {}'.format(signed_message_wrong))
    
    ####################################
    with reverts("Ownable: caller is not the owner"):
        NFTMinter1155Uni.setSignerStatus(ORACLE_ADDRESS, True, {'from':accounts[1]})

    with reverts("Ownable: caller is not the owner"):
        NFTMinter1155Uni.setNeedCheckSign(True, {"from": accounts[1]})

    NFTMinter1155Uni.setNeedCheckSign(True, {"from": accounts[0]})

    with reverts("Unexpected signer"):
        NFTMinter1155Uni.mintWithURI(accounts[1], tokenId, amount, tokenUri, signed_message.signature, {"from": accounts[0]})

    NFTMinter1155Uni.setSignerStatus(ORACLE_ADDRESS, True, {'from':accounts[0]})

    #wrong signature
    with reverts("Unexpected signer"):
        NFTMinter1155Uni.mintWithURI(accounts[1], tokenId, amount, tokenUri, signed_message_wrong.signature, {"from": accounts[0]})

    tx = NFTMinter1155Uni.mintWithURI(accounts[1], tokenId, amount, tokenUri, signed_message.signature, {"from": accounts[0]})
    logging.info('gas = {}'.format(tx.gas_used))

    #use previous data again
    with reverts("This id already minted"):
        NFTMinter1155Uni.mintWithURI(accounts[1], tokenId, amount, tokenUri, signed_message.signature, {"from": accounts[0]})

    with reverts("Unexpected signer"):
        NFTMinter1155Uni.mintWithURI(accounts[1], tokenId+1, amount, tokenUri, signed_message.signature, {"from": accounts[0]})
    assert NFTMinter1155Uni.totalSupply(tokenId) == amount
    assert NFTMinter1155Uni.balanceOf(accounts[1], tokenId) == amount

    logging.info('uri = {}'.format(NFTMinter1155Uni.uri(1)))

def test_batch(accounts, NFTMinter1155Uni):
    #with signature
    _to = [accounts[1].address, accounts[2].address]
    _tokenId = [3, 4]
    _amounts = [1, 1]
    _tokenURI = ['3', '4']
    _signature = []

    for i in range(2):
        encoded_msg = encode_single(
            '(address,uint256,string,uint256)',
            ( accounts[0].address, 
            Web3.toInt(_tokenId[i]) ,
            _tokenURI[i],
            Web3.toInt(_amounts[i])
            )
        )

        hashed_msg = Web3.solidityKeccak(['bytes32'], [encoded_msg])
        logging.info('hashed_msg = {}'.format(hashed_msg))
        # Ether style signature
        message = encode_defunct(primitive=hashed_msg)
        logging.info('message = {}'.format(message))
        signed_message = web3.eth.account.sign_message(message, private_key=ORACLE_PRIVATE_KEY)
        
        logging.info('sign_message is {}'.format(signed_message))

        _signature.append(signed_message.signature)
           
    NFTMinter1155Uni.mintWithURIBatch(
                                        _to, 
                                        _tokenId, 
                                        _amounts, 
                                        _tokenURI, 
                                        _signature
                                    )

    assert NFTMinter1155Uni.balanceOf(accounts[1], 3) == 1
   