import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://api.envelop.is/metadata/'


def test_minter_1(accounts, NFTMinter1155UniV2):  

    tx = NFTMinter1155UniV2.mintWithURI(accounts[2], 1, 3, '', {'from':accounts[1]})
    tokenId = tx.events['TransferSingle']['id']

    assert NFTMinter1155UniV2.name() == "Envelop NFT Uni Storage"
    assert NFTMinter1155UniV2.balanceOf(accounts[2], tokenId) == 3

    
    tx = NFTMinter1155UniV2.mintWithURIBatch(
                                    [accounts[3], accounts[4]],
                                    [2, 3],
                                    [5, 6],
                                    ['', ''],
                                    {"from": accounts[1]}

                                )

    assert NFTMinter1155UniV2.balanceOf(accounts[3], 2) == 5
    assert NFTMinter1155UniV2.balanceOf(accounts[4], 3) == 6
    assert NFTMinter1155UniV2.balanceOfBatch([accounts[3], accounts[4]], [2,3]) == [5,6]

    #check approve
    NFTMinter1155UniV2.setApprovalForAll(accounts[9], True, {"from": accounts[3]})
    assert NFTMinter1155UniV2.isApprovedForAll(accounts[3], accounts[9]) == True
    #check transfer
    NFTMinter1155UniV2.safeTransferFrom(accounts[3], accounts[5], 2, 1, '', {"from": accounts[3]})
    assert NFTMinter1155UniV2.balanceOf(accounts[5], 2) == 1
    NFTMinter1155UniV2.safeBatchTransferFrom(
                                                accounts[3], 
                                                accounts[6], 
                                                [2],
                                                [2],
                                                '',
                                                {"from": accounts[3]}
                                            )

    assert NFTMinter1155UniV2.balanceOf(accounts[6], 2) == 2
    assert NFTMinter1155UniV2.balanceOf(accounts[3], 2) == 2

    NFTMinter1155UniV2.burn(2, 2, {"from": accounts[3]})
    assert NFTMinter1155UniV2.balanceOf(accounts[3], 2) == 0
        
        

def test_minter_2(accounts, NFTMinter1155UniV2):
   
    #with tokenURI
    tx = NFTMinter1155UniV2.mintWithURI(accounts[3], 4, 4, 'xxx', {'from':accounts[2]})    
    assert NFTMinter1155UniV2.balanceOf(accounts[3], 4) == 4
    assert NFTMinter1155UniV2.totalSupply(4) == 4

    assert NFTMinter1155UniV2.exists(4) == True
    #check tokenUri
    logging.info(NFTMinter1155UniV2.uri(4))
    logging.info(chain.id)
    assert (NFTMinter1155UniV2.uri(4)).lower() == (baseUrl+str(1)+'/'+NFTMinter1155UniV2.address+'/'+'xxx').lower()
    
    ipfs_uri =  'ipfs://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinter1155UniV2.mintWithURI(accounts[3], 5, 4, ipfs_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(5, NFTMinter1155UniV2.uri(5)))
    
    swarm_uri =  'bzz://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinter1155UniV2.mintWithURI(accounts[3], 6, 4, swarm_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(6, NFTMinter1155UniV2.uri(6)))

    tx = NFTMinter1155UniV2.mintWithURI(accounts[3], 7, 4, '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(7, NFTMinter1155UniV2.uri(7)))

    # Checks before seting prefix
    assert NFTMinter1155UniV2.uri(5) == ipfs_uri
    assert NFTMinter1155UniV2.uri(6) == swarm_uri

    with reverts("Ownable: caller is not the owner"):
        NFTMinter1155UniV2.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[3]})

    NFTMinter1155UniV2.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[0]})
    NFTMinter1155UniV2.setPrefixURI('ipfs', 'https://envelop.is/ipfs/', {"from": accounts[0]})
    logging.info('tokenURI({})={}'.format(5, NFTMinter1155UniV2.uri(5)))
    logging.info('tokenURI({})={}'.format(6, NFTMinter1155UniV2.uri(6)))
    logging.info('tokenURI({})={}'.format(7, NFTMinter1155UniV2.uri(7)))

    # Checks after setting prefix
    assert NFTMinter1155UniV2.uri(5) == 'https://envelop.is/ipfs/' + ipfs_uri[7:]
    assert NFTMinter1155UniV2.uri(6) == 'https://swarm.envelop.is/bzz/' + swarm_uri[6:]
    assert NFTMinter1155UniV2.uri(7) == (baseUrl+str(1)+'/'+NFTMinter1155UniV2.address+'/'+'7').lower()


    
    
    
    


