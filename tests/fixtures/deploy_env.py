import pytest

@pytest.fixture(scope="module")
def NFTMinterUni(accounts, EnvelopUsers721UniStorageEnumV1):
    NFTMinter = accounts[0].deploy(EnvelopUsers721UniStorageEnumV1,
        "Envelop NFT Uni Storage", 
        "eNFT", 
        "https://api.envelop.is/metadata/"
    )
    yield NFTMinter    

@pytest.fixture(scope="module")
def NFTMinter1155Uni(accounts, EnvelopUsers1155UniStorageV1):
    NFTMinter1155 = accounts[0].deploy(EnvelopUsers1155UniStorageV1,
        "Envelop NFT Uni Storage", 
        "eNFT", 
        "https://api.envelop.is/metadata/"
    )
    yield NFTMinter1155

@pytest.fixture(scope="module")
def NFTMinterUniV2(accounts, EnvelopUsers721UniStorageEnumV2):
    NFTMinterUniV2 = accounts[0].deploy(EnvelopUsers721UniStorageEnumV2,
        "Envelop NFT Uni Storage", 
        "eNFT", 
        "https://api.envelop.is/metadata/"
    )
    yield NFTMinterUniV2    

@pytest.fixture(scope="module")
def NFTMinter1155UniV2(accounts, EnvelopUsers1155UniStorageV2):
    NFTMinter1155UniV2 = accounts[0].deploy(EnvelopUsers1155UniStorageV2,
        "Envelop NFT Uni Storage", 
        "eNFT", 
        "https://api.envelop.is/metadata/"
    )
    yield NFTMinter1155UniV2