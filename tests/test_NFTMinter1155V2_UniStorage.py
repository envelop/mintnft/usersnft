import pytest
import logging
from brownie import Wei, reverts, chain, web3
from web3 import Web3
from eth_abi import encode_single
from eth_account.messages import encode_defunct


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'

def test_mint(accounts, NFTMinter1155UniV2):
    
    tokenId = 1
    amount = 3
    tokenUri = 'b72f05424ee87a65cb7c94b432d3b5b553bbb82f7b0fe34e8a3ad161b1b05ca5/'
    
    tx = NFTMinter1155UniV2.mintWithURI(accounts[1], tokenId, amount, tokenUri, {"from": accounts[0]})
    logging.info('gas = {}'.format(tx.gas_used))

    
    NFTMinter1155UniV2.mintWithURI(accounts[1], tokenId+1, amount, tokenUri, {"from": accounts[0]})
    assert NFTMinter1155UniV2.totalSupply(tokenId) == amount
    assert NFTMinter1155UniV2.balanceOf(accounts[1], tokenId) == amount

    logging.info('uri = {}'.format(NFTMinter1155UniV2.uri(1)))

def test_batch(accounts, NFTMinter1155UniV2):
    _to = [accounts[1].address, accounts[2].address]
    _tokenId = [3, 4]
    _amounts = [1, 1]
    _tokenURI = ['3', '4']

    NFTMinter1155UniV2.mintWithURIBatch(
                                        _to, 
                                        _tokenId, 
                                        _amounts, 
                                        _tokenURI
                                    )

    assert NFTMinter1155UniV2.balanceOf(accounts[1], 3) == 1
   