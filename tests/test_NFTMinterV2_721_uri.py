import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://api.envelop.is/metadata/'


def test_minter_1(accounts, NFTMinterUniV2):  


    tx = NFTMinterUniV2.mintWithURI(accounts[2], '', {'from':accounts[1]})
    tokenId = tx.events['Transfer']['tokenId']

    assert NFTMinterUniV2.name() == "Envelop NFT Uni Storage"
    assert NFTMinterUniV2.ownerOf(0) == accounts[2]

    tx = NFTMinterUniV2.mintWithURIBatch(
                                            [accounts[3], accounts[4]],
                                            ['', ''],
                                            {"from": accounts[1]}

                                        )

    assert NFTMinterUniV2.ownerOf(1) == accounts[3]
    assert NFTMinterUniV2.ownerOf(2) == accounts[4]
    assert NFTMinterUniV2.balanceOf(accounts[4]) == 1

    #check approve
    NFTMinterUniV2.approve(accounts[9], 1, {"from": accounts[3]})
    assert NFTMinterUniV2.getApproved(1) ==  accounts[9]
    #check transfer
    NFTMinterUniV2.transferFrom(accounts[3], accounts[5], 1, {"from": accounts[9]})
    assert NFTMinterUniV2.ownerOf(1) == accounts[5]

def test_minter_2(accounts, NFTMinterUniV2):
    

    #with tokenURI
    tx = NFTMinterUniV2.mintWithURI(accounts[3], 'xxx', {'from':accounts[2]})    
    

    #check tokenURI
    logging.info(NFTMinterUniV2.tokenURI(3))
    logging.info(chain.id)
    assert (NFTMinterUniV2.tokenURI(3)).lower() == (baseUrl+str(1)+'/'+NFTMinterUniV2.address+'/'+'xxx').lower()
    
    ipfs_uri =  'ipfs://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinterUniV2.mintWithURI(accounts[3], ipfs_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(4, NFTMinterUniV2.tokenURI(4)))
    
    swarm_uri =  'bzz://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinterUniV2.mintWithURI(accounts[3], swarm_uri, {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(5, NFTMinterUniV2.tokenURI(5)))

    tx = NFTMinterUniV2.mintWithURI(accounts[3], '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(6, NFTMinterUniV2.tokenURI(6)))

    # Checks before setting prefix
    assert NFTMinterUniV2.tokenURI(4) == ipfs_uri
    assert NFTMinterUniV2.tokenURI(5) == swarm_uri

    with reverts("Ownable: caller is not the owner"):
        NFTMinterUniV2.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[3]})

    NFTMinterUniV2.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[0]})
    NFTMinterUniV2.setPrefixURI('ipfs', 'https://envelop.is/ipfs/', {"from": accounts[0]})
    logging.info('tokenURI({})={}'.format(4, NFTMinterUniV2.tokenURI(4)))
    logging.info('tokenURI({})={}'.format(5, NFTMinterUniV2.tokenURI(5)))
    logging.info('tokenURI({})={}'.format(6, NFTMinterUniV2.tokenURI(6)))

    # Checks after setting prefix
    assert NFTMinterUniV2.tokenURI(4) == 'https://envelop.is/ipfs/' + ipfs_uri[7:]
    assert NFTMinterUniV2.tokenURI(5) == 'https://swarm.envelop.is/bzz/' + swarm_uri[6:]
    assert NFTMinterUniV2.tokenURI(6) == (baseUrl+str(1)+'/'+NFTMinterUniV2.address+'/'+'6').lower()


    
    
    
    


