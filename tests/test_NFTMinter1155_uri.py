import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://api.envelop.is/metadata/'


def test_minter_1(accounts, NFTMinter1155Uni):  

    tx = NFTMinter1155Uni.mintWithURI(accounts[2], 1, 3, '', '', {'from':accounts[1]})
    tokenId = tx.events['TransferSingle']['id']

    assert NFTMinter1155Uni.name() == "Envelop NFT Uni Storage"
    assert NFTMinter1155Uni.balanceOf(accounts[2], tokenId) == 3

    
    tx = NFTMinter1155Uni.mintWithURIBatch(
                                    [accounts[3], accounts[4]],
                                    [2, 3],
                                    [5, 6],
                                    ['', ''],
                                    ['', ''],
                                    {"from": accounts[1]}

                                )

    assert NFTMinter1155Uni.balanceOf(accounts[3], 2) == 5
    assert NFTMinter1155Uni.balanceOf(accounts[4], 3) == 6
    assert NFTMinter1155Uni.balanceOfBatch([accounts[3], accounts[4]], [2,3]) == [5,6]

    #check approve
    NFTMinter1155Uni.setApprovalForAll(accounts[9], True, {"from": accounts[3]})
    assert NFTMinter1155Uni.isApprovedForAll(accounts[3], accounts[9]) == True
    #check transfer
    NFTMinter1155Uni.safeTransferFrom(accounts[3], accounts[5], 2, 1, '', {"from": accounts[3]})
    assert NFTMinter1155Uni.balanceOf(accounts[5], 2) == 1
    NFTMinter1155Uni.safeBatchTransferFrom(
                                                accounts[3], 
                                                accounts[6], 
                                                [2],
                                                [2],
                                                '',
                                                {"from": accounts[3]}
                                            )

    assert NFTMinter1155Uni.balanceOf(accounts[6], 2) == 2
    assert NFTMinter1155Uni.balanceOf(accounts[3], 2) == 2

    NFTMinter1155Uni.burn(2, 2, {"from": accounts[3]})
    assert NFTMinter1155Uni.balanceOf(accounts[3], 2) == 0
        
        

def test_minter_2(accounts, NFTMinter1155Uni):
   
    #with tokenURI
    tx = NFTMinter1155Uni.mintWithURI(accounts[3], 4, 4, 'xxx', '', {'from':accounts[2]})    
    assert NFTMinter1155Uni.balanceOf(accounts[3], 4) == 4
    assert NFTMinter1155Uni.totalSupply(4) == 4

    assert NFTMinter1155Uni.exists(4) == True
    #check tokenUri
    logging.info(NFTMinter1155Uni.uri(4))
    logging.info(chain.id)
    assert (NFTMinter1155Uni.uri(4)).lower() == (baseUrl+str(1)+'/'+NFTMinter1155Uni.address+'/'+'xxx').lower()
    
    ipfs_uri =  'ipfs://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinter1155Uni.mintWithURI(accounts[3], 5, 4, ipfs_uri, '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(5, NFTMinter1155Uni.uri(5)))
    
    swarm_uri =  'bzz://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinter1155Uni.mintWithURI(accounts[3], 6, 4, swarm_uri, '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(6, NFTMinter1155Uni.uri(6)))

    tx = NFTMinter1155Uni.mintWithURI(accounts[3], 7, 4, '', '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(7, NFTMinter1155Uni.uri(7)))

    # Checks before seting prefix
    assert NFTMinter1155Uni.uri(5) == ipfs_uri
    assert NFTMinter1155Uni.uri(6) == swarm_uri

    with reverts("Ownable: caller is not the owner"):
        NFTMinter1155Uni.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[3]})

    NFTMinter1155Uni.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[0]})
    NFTMinter1155Uni.setPrefixURI('ipfs', 'https://envelop.is/ipfs/', {"from": accounts[0]})
    logging.info('tokenURI({})={}'.format(5, NFTMinter1155Uni.uri(5)))
    logging.info('tokenURI({})={}'.format(6, NFTMinter1155Uni.uri(6)))
    logging.info('tokenURI({})={}'.format(7, NFTMinter1155Uni.uri(7)))

    # Checks after setting prefix
    assert NFTMinter1155Uni.uri(5) == 'https://envelop.is/ipfs/' + ipfs_uri[7:]
    assert NFTMinter1155Uni.uri(6) == 'https://swarm.envelop.is/bzz/' + swarm_uri[6:]
    assert NFTMinter1155Uni.uri(7) == (baseUrl+str(1)+'/'+NFTMinter1155Uni.address+'/'+'7').lower()


    
    
    
    


