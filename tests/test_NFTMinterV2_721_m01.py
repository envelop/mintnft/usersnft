import pytest
import logging
from brownie import Wei, reverts, chain, web3
from web3 import Web3
from eth_abi import encode_single
from eth_account.messages import encode_defunct


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
timelockPeriod = 3600*24*30*12 #1 year
ticketValidPeriod = 10  #10 sec
counter = 0
payAmount = 1e18
in_type = 3
out_type = 3
in_nft_amount = 3



def test_mint(accounts, NFTMinterUniV2):
    
    tokenUri = 'b72f05424ee87a65cb7c94b432d3b5b553bbb82f7b0fe34e8a3ad161b1b05ca5/'
    tx = NFTMinterUniV2.mintWithURI(accounts[1], tokenUri, {"from": accounts[0]})
    logging.info('gas = {}'.format(tx.gas_used))
    assert NFTMinterUniV2.ownerOf(NFTMinterUniV2.totalSupply()-1) ==accounts[1]


    #mint again
    NFTMinterUniV2.mintWithURI(accounts[1],tokenUri, {"from": accounts[0]})
    assert NFTMinterUniV2.ownerOf(NFTMinterUniV2.totalSupply()-1) ==accounts[1]

def test_batch(accounts, NFTMinterUniV2):
    #with signature
    _to = [accounts[1].address, accounts[2].address]
    _tokenURI = ['2', '3']
           
    NFTMinterUniV2.mintWithURIBatch(_to, _tokenURI,  {'from':accounts[0]})

    assert  NFTMinterUniV2.ownerOf(NFTMinterUniV2.totalSupply()-1) ==accounts[2]





