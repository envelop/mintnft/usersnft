import pytest
import logging
from brownie import Wei, reverts, chain, web3, Contract
from web3 import Web3


LOGGER = logging.getLogger(__name__)

zero_address = '0x0000000000000000000000000000000000000000'
baseUrl = 'https://api.envelop.is/metadata/'


def test_minter_1(accounts, NFTMinterUni):  


    tx = NFTMinterUni.mintWithURI(accounts[2], 1, '', '', {'from':accounts[1]})
    tokenId = tx.events['Transfer']['tokenId']

    assert NFTMinterUni.name() == "Envelop NFT Uni Storage"
    assert NFTMinterUni.ownerOf(1) == accounts[2]

    tx = NFTMinterUni.mintWithURIBatch(
                                            [accounts[3], accounts[4]],
                                            [2, 3],
                                            ['', ''],
                                            ['', ''],
                                            {"from": accounts[1]}

                                        )

    assert NFTMinterUni.ownerOf(2) == accounts[3]
    assert NFTMinterUni.ownerOf(3) == accounts[4]
    assert NFTMinterUni.balanceOf(accounts[4]) == 1

    #check approve
    NFTMinterUni.approve(accounts[9], 2, {"from": accounts[3]})
    assert NFTMinterUni.getApproved(2) ==  accounts[9]
    #check transfer
    NFTMinterUni.transferFrom(accounts[3], accounts[5], 2, {"from": accounts[9]})
    assert NFTMinterUni.ownerOf(2) == accounts[5]

    NFTMinterUni.burn(2,{"from": accounts[5]})
    with reverts("ERC721: invalid token ID"):
        assert NFTMinterUni.ownerOf(2) == accounts[5]
        
        

def test_minter_2(accounts, NFTMinterUni):
    

    #with tokenURI
    tx = NFTMinterUni.mintWithURI(accounts[3], 4, 'xxx', '', {'from':accounts[2]})    
    

    #check tokenURI
    logging.info(NFTMinterUni.tokenURI(1))
    logging.info(chain.id)
    assert (NFTMinterUni.tokenURI(4)).lower() == (baseUrl+str(1)+'/'+NFTMinterUni.address+'/'+'xxx').lower()
    
    ipfs_uri =  'ipfs://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinterUni.mintWithURI(accounts[3], 5, ipfs_uri, '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(5, NFTMinterUni.tokenURI(5)))
    
    swarm_uri =  'bzz://bafkreicku24z2763a6eu4kjhqa64p63zvltepfctnyf3unmtudagfhiyby'
    tx = NFTMinterUni.mintWithURI(accounts[3], 6, swarm_uri, '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(6, NFTMinterUni.tokenURI(6)))

    tx = NFTMinterUni.mintWithURI(accounts[3], 7, '', '', {'from':accounts[2]})    
    logging.info('tokenURI({})={}'.format(7, NFTMinterUni.tokenURI(7)))

    # Checks before setting prefix
    assert NFTMinterUni.tokenURI(5) == ipfs_uri
    assert NFTMinterUni.tokenURI(6) == swarm_uri

    with reverts("Ownable: caller is not the owner"):
        NFTMinterUni.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[3]})

    NFTMinterUni.setPrefixURI('bzz', 'https://swarm.envelop.is/bzz/', {"from": accounts[0]})
    NFTMinterUni.setPrefixURI('ipfs', 'https://envelop.is/ipfs/', {"from": accounts[0]})
    logging.info('tokenURI({})={}'.format(5, NFTMinterUni.tokenURI(5)))
    logging.info('tokenURI({})={}'.format(6, NFTMinterUni.tokenURI(6)))
    logging.info('tokenURI({})={}'.format(7, NFTMinterUni.tokenURI(7)))

    # Checks after setting prefix
    assert NFTMinterUni.tokenURI(5) == 'https://envelop.is/ipfs/' + ipfs_uri[7:]
    assert NFTMinterUni.tokenURI(6) == 'https://swarm.envelop.is/bzz/' + swarm_uri[6:]
    assert NFTMinterUni.tokenURI(7) == (baseUrl+str(1)+'/'+NFTMinterUni.address+'/'+'7').lower()


    
    
    
    


