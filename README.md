## Foundry

**Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.**

Foundry consists of:

-   **Forge**: Ethereum testing framework (like Truffle, Hardhat and DappTools).
-   **Cast**: Swiss army knife for interacting with EVM smart contracts, sending transactions and getting chain data.
-   **Anvil**: Local Ethereum node, akin to Ganache, Hardhat Network.
-   **Chisel**: Fast, utilitarian, and verbose solidity REPL.

## Documentation

https://book.getfoundry.sh/

## Usage
### Clone with submodules
```bash
git clone git@gitlab.com:ubd2/ubd-market-adapters.git
git submodule update --init --recursive
```

### Build

```shell
$ forge build
```

### Test

```shell
$ forge test
```

### Format

```shell
$ forge fmt
```

### Gas Snapshots

```shell
$ forge snapshot
```

### Anvil

```shell
$ anvil
```
https://red-billowing-sanctuary.blast-sepolia.quiknode.pro/8c92c393b7ed9f600550841a38707969ffbbac40/
### Deploy

```shell
$ source .envx``
$ forge script script/Counter.s.sol:CounterScript --rpc-url <your_rpc_url> --private-key <your_private_key>

#### Deploy Sepolia
$ forge script script/Deploy.s.sol:DeployScript --rpc-url https://sepolia.infura.io/v3/$WEB3_INFURA_PROJECT_ID --private-key $DEV_PRIVATE_KEY --verify --etherscan-api-key $ETHERSCAN_TOKEN --broadcast


#### Deploy Blast
$ forge script script/Deploy.s.sol:DeployScript --rpc-url https://red-billowing-sanctuary.blast-sepolia.quiknode.pro/$WEB3_QUICKNODE_ID/ --private-key $DEV_PRIVATE_KEY --verify --verifier-url 'https://api.routescan.io/v2/network/testnet/evm/168587773/etherscan' --etherscan-api-key "verifyContract" --broadcast
# Use for testnet
--account ttwo --sender 0xDDA2F2E159d2Ce413Bd0e1dF5988Ee7A803432E3 

# Use for Mainnet
--account envdeployer  

#### Onchain simulation
$ forge script script/Deploy.s.sol:DeployScript --rpc-url https://sepolia.infura.io/v3/$WEB3_INFURA_PROJECT_ID
$ forge script script/Deploy.s.sol:DeployScript --rpc-url https://red-billowing-sanctuary.blast-sepolia.quiknode.pro/$WEB3_QUICKNODE_ID/
$ forge script script/Deploy.s.sol:DeployScript --rpc-url blast_sepolia 

#### Debuging
$ forge script script/Deploy.s.sol:DeployScript 
```
#### Verify
```shell
$ forge verify-contract 0xB980e1C92E14c1ceaD2ecA30475e659B81d7bb22  ./contracts/EnvelopUsers721UniStorageEnumV2.sol:EnvelopUsers721UniStorageEnumV2 --verifier-url 'https://api.routescan.io/v2/network/testnet/evm/168587773/etherscan' --etherscan-api-key "verifyContract" --num-of-optimizations 200 --compiler-version 0.8.21 --constructor-args $(cast abi-encode "constructor(string name_, string symbol_, string _baseurl)" 'Envelop Users NFT 721' 'ENVELOP' 'https://api.envelop.is/metadata/')

$ forge verify-contract  0x63108e6F8A40c3a4c71a9bf118c706cD3767ADCF ./contracts/EnvelopUsers1155UniStorageV2.sol:EnvelopUsers1155UniStorageV2 --verifier-url 'https://api.routescan.io/v2/network/testnet/evm/168587773/etherscan' --etherscan-api-key "verifyContract" --num-of-optimizations 200 --compiler-version 0.8.21 --constructor-args $(cast abi-encode "constructor(string name_, string symbol_, string _baseurl)" 'Envelop Users NFT 1155' 'ENVELOP' 'https://api.envelop.is/metadata/')
```
#### Deploy Blast Mainnet

```shell
$ forge script script/DeployBlast.s.sol:DeployScriptBlast --rpc-url blast_mainnet  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast
```
##### Verify Blast Mainnet
```shell
$ forge verify-contract 0x6426Bc3F86E554f44bE7798c8D1f3482Fb7BB68C  ./contracts/EnvelopUsers721UniStorageEnumV2WithBlastPoints.sol:EnvelopUsers721UniStorageEnumV2WithBlastPoints --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21 --constructor-args $(cast abi-encode "constructor(string name_, string symbol_, string _baseurl, address _pointsOperator)" 'Envelop Users NFT 721' 'ENVELOP' 'https://api.envelop.is/metadata/' 0x303cD2A927D9Cb6F5CE03b88a4e3E2528baEDF40)

$ forge verify-contract  0xc2571eBbc8F2af4f832bB8a2D3A4b0932Ce24773 ./contracts/EnvelopUsers1155UniStorageV2WithBlastPoints.sol:EnvelopUsers1155UniStorageV2WithBlastPoints --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21 --constructor-args $(cast abi-encode "constructor(string name_, string symbol_, string _baseurl, address _pointsOperator)" 'Envelop Users NFT 1155' 'ENVELOP' 'https://api.envelop.is/metadata/' 0x303cD2A927D9Cb6F5CE03b88a4e3E2528baEDF40)
```

#### Deploy Optimism Mainnet

```shell
$ forge script script/Deploy.s.sol:DeployScript --rpc-url optimism  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast --verify  --etherscan-api-key $API_KEY_OPTIMISTIC_ETHERSCAN
```

#### Deploy Ethereum Sepolia MyShch
```shell
$ forge script script/DeployMyshch.s.sol:DeployScript --rpc-url sepolia  --account ttwo --sender 0xDDA2F2E159d2Ce413Bd0e1dF5988Ee7A803432E3 --broadcast --verify  --etherscan-api-key $ETHERSCAN_TOKEN
```
#### Deploy MyShch
```shell
$ # Ethereum
$ forge script script/DeployMyshch.s.sol:DeployScript --rpc-url mainnet  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast --verify  --etherscan-api-key $ETHERSCAN_TOKEN

$ # Optimism
$ forge script script/DeployMyshch.s.sol:DeployScript --rpc-url optimism  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast --verify  --etherscan-api-key $API_KEY_OPTIMISTIC_ETHERSCAN

$ # Arbitrum
$ forge script script/DeployMyshch.s.sol:DeployScript --rpc-url arbitrum  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast --verify  --etherscan-api-key $ARBISCAN_TOKEN

$ # polygon
$ forge script script/DeployMyshch.s.sol:DeployScript --rpc-url polygon  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast --verify  --etherscan-api-key $POLYGONSCAN_TOKEN

$ # BSC
$ forge script script/DeployMyshch.s.sol:DeployScript --rpc-url bnb_smart_chain  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast --verify  --etherscan-api-key $BSCSCAN_TOKEN


$ # Blast
$ forge script script/DeployMyshchBlast.s.sol:DeployScript --rpc-url blast_mainnet  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast --verify  --etherscan-api-key $BLASTSCAN_TOKEN
```


### Cast

```shell
$ cast <subcommand>
```

### Help

```shell
$ forge --help
$ anvil --help
$ cast --help
```
