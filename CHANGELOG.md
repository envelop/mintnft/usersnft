
# CHANGELOG

All notable changes to this project are documented in this file.

This changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- TokenId autoincrement for 1155

## [0.0.2](https://gitlab.com/envelop/mintnft/usersnft/-/tags/0.0.2) - 2023-09-02
### Added
- Upgrade solidity version up to 0.8.21
- Open Zeppelin dependencies upgrade to 4.9.3
- Remove signer support 
- Added auto increment tokenId 721
- Remove burn from public iplementation 721

## [0.0.1](https://gitlab.com/envelop/mintnft/usersnft/-/tags/0.0.1) - 2023-03-24
### Added
- Upgrade solidity version up to 0.8.19
- Open Zeppelin dependencies upgrade to 4.8.3
- Support diferent base URIs for distributed storage (IPFS,SWARM etc)

