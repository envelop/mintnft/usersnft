## Myshch Public Collection in Optimizm  2024-11-22
  **EnvelopUsers721UniStorageEnumV2**
  https://optimistic.etherscan.io/address/0x844A8AE519f9bf28dE9211d16E417A7710a25AA1#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0x844A8AE519f9bf28dE9211d16E417A7710a25AA1')
  ```

## Myshch Public Collection in Arbitrum  2024-11-22
  **EnvelopUsers721UniStorageEnumV2**
  https://arbiscan.io/address/0x282B973817Dc7d149b05474b848610Fc70Bf4CF2#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0x282B973817Dc7d149b05474b848610Fc70Bf4CF2')
  ```

## Myshch Public Collection in BSC  2024-11-22
  **EnvelopUsers721UniStorageEnumV2**
  https://bscscan.com/address/0xDE549f6DC7A7e1A0809a4282B76d5914D52e4f20#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0xDE549f6DC7A7e1A0809a4282B76d5914D52e4f20')
  ```


## Myshch Public Collection in Polygon  2024-11-22
  **EnvelopUsers721UniStorageEnumV2**
  https://polygonscan.com/address/0x1c3773f3e23A1659f81A5ef13E26ec561f887dcF#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0x1c3773f3e23A1659f81A5ef13E26ec561f887dcF')
  ```

## Myshch Public Collection in Blast  2024-11-22
  **EnvelopUsers721UniStorageEnumV2**
  https://blastscan.io/address/0xce0d9Ec8351b0D230Dc5f72f0f804b206c28D3d0#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0xce0d9Ec8351b0D230Dc5f72f0f804b206c28D3d0')
  ```


## Myshch Public Collection in Ethereum  2024-11-22
  **EnvelopUsers721UniStorageEnumV2**
  https://etherscan.io/address/0x547D5018E185A152852Def86f51C9ff90475dCc8#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0x547D5018E185A152852Def86f51C9ff90475dCc8')
  ```


## Myshch Public Collection in Sepolia  2024-11-21
  **EnvelopUsers721UniStorageEnumV2**
  https://sepolia.etherscan.io/address/0x4166ffC4554Af4DAcf0615f227CC739E4dF374C3#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0x4166ffC4554Af4DAcf0615f227CC739E4dF374C3')
  ```

## Envelop User minters V2 in Optimism Mainnet 2024-07-14
  **EnvelopUsers721UniStorageEnumV2**
  https://optimistic.etherscan.io/address/0xF1CD019d962eF5f54BF58af561365b9680e1191D#code

  **EnvelopUsers1155UniStorageV2**
  https://optimistic.etherscan.io/address/0x92354FCCdd7E3f6415a4Fb87d080A62D8a58144A#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0xF1CD019d962eF5f54BF58af561365b9680e1191D')
  mint1155 = EnvelopUsers1155UniStorageV2.at('0x92354FCCdd7E3f6415a4Fb87d080A62D8a58144A')
  ```

## Envelop User minters V2 in Blast Mainnet 2024-03-03

  **EnvelopUsers721UniStorageEnumV2WithBlastPoints**
  https://blastscan.io/address/0x6426Bc3F86E554f44bE7798c8D1f3482Fb7BB68C#code

  **EnvelopUsers1155UniStorageV2WithBlastPoints**
  https://blastscan.io/address/0xc2571eBbc8F2af4f832bB8a2D3A4b0932Ce24773#code

  ```python
  mint721 = EnvelopUsers721UniStorageEnumV2WithBlastPoints.at('0x6426Bc3F86E554f44bE7798c8D1f3482Fb7BB68C')
  mint1155 = EnvelopUsers1155UniStorageV2WithBlastPoints.at('0xc2571eBbc8F2af4f832bB8a2D3A4b0932Ce24773')
  ```

## Envelop User minters V2 in Blast Sepolia TestNet 2024-01-21
**EnvelopUsers721UniStorageEnumV2**
https://testnet.blastscan.io/address/0xB980e1C92E14c1ceaD2ecA30475e659B81d7bb22#code

**EnvelopUsers1155UniStorageV2**
https://testnet.blastscan.io/address/0x63108e6F8A40c3a4c71a9bf118c706cD3767ADCF#code

```python
  mint721 = EnvelopUsers721UniStorageEnumV2.at('0xB980e1C92E14c1ceaD2ecA30475e659B81d7bb22')
  mint1155 = EnvelopUsers1155UniStorageV2.at('0x63108e6F8A40c3a4c71a9bf118c706cD3767ADCF')
```
## Envelop User minters V2 in Scroll MainNet 2023-11-03
**EnvelopUsers721UniStorageEnumV2**
https://scrollscan.com/address/0xc2571eBbc8F2af4f832bB8a2D3A4b0932Ce24773#code

**EnvelopUsers1155UniStorageV2**
https://scrollscan.com/address/0x6426Bc3F86E554f44bE7798c8D1f3482Fb7BB68C#code

----------Deployment artifacts-------------------
```python
mint721 = EnvelopUsers721UniStorageEnumV2.at('0xc2571eBbc8F2af4f832bB8a2D3A4b0932Ce24773')
mint1155 = EnvelopUsers1155UniStorageV2.at('0x6426Bc3F86E554f44bE7798c8D1f3482Fb7BB68C')
```


## Envelop User minters V2 in Ethereum MainNet 2023-11-01
**EnvelopUsers721UniStorageEnumV2**
https://etherscan.io/address/0xbFb8A10EA886dE9269Cc890fD22785330b323B2b#code

**EnvelopUsers1155UniStorageV2**
https://etherscan.io/address/0xC8095B46819Ee26a792BD1576D85153A0E40f1df#code

----------Deployment artifacts-------------------
```python
mint721 = EnvelopUsers721UniStorageEnumV2.at('0xbFb8A10EA886dE9269Cc890fD22785330b323B2b')
mint1155 = EnvelopUsers1155UniStorageV2.at('0xC8095B46819Ee26a792BD1576D85153A0E40f1df')
```


## Envelop User minters V2 in Arbitrum MainNet 2023-11-01
**EnvelopUsers721UniStorageEnumV2**
https://arbiscan.io/address/0x3F4deAC77d3aEb6aF2B827Cf8B0e772239F2D3e9#code

**EnvelopUsers1155UniStorageV2**
https://arbiscan.io/address/0xF086ed4652A6d80CBEbB5A6d6Bb26A3C239c0ecC#code

----------Deployment artifacts-------------------
```python
mint721 = EnvelopUsers721UniStorageEnumV2.at('0x3F4deAC77d3aEb6aF2B827Cf8B0e772239F2D3e9')
mint1155 = EnvelopUsers1155UniStorageV2.at('0xF086ed4652A6d80CBEbB5A6d6Bb26A3C239c0ecC')
```



## Envelop User minters V2 in Polygon MainNet 2023-10-31

**EnvelopUsers721UniStorageEnumV2**
https://polygonscan.com/address/0x721f94A6669ADF57eD100Fe1a914B94aCd0aDC23#code

**EnvelopUsers1155UniStorageV2**
https://polygonscan.com/address/0x36Ede4b55b226c69Abf01826f50107455B390E3c#code

----------Deployment artifacts-------------------
```python
mint721 = EnvelopUsers721UniStorageEnumV2.at('0x721f94A6669ADF57eD100Fe1a914B94aCd0aDC23')
mint1155 = EnvelopUsers1155UniStorageV2.at('0x36Ede4b55b226c69Abf01826f50107455B390E3c')
```
## Envelop User minters V2 in BSC MainNet 2023-10-26
https://bscscan.com/address/0x0C40D8C0BA6A8B97cF3a43Bf34C61e2b3299984b#code
https://bscscan.com/address/0x1F90B651Ddb295efd24F7dC9D273461212c9Ff8A#code

```python
mint721 = EnvelopUsers721UniStorageEnumV2.at('0x0C40D8C0BA6A8B97cF3a43Bf34C61e2b3299984b')
mint1155 = EnvelopUsers1155UniStorageV2.at('0x1F90B651Ddb295efd24F7dC9D273461212c9Ff8A')
```
## Envelop User minters V2 in Sepolia Testnet 2023-09-02

https://sepolia.etherscan.io/address/0x1bbc7e5B329b4C79505309ABFe0c03455D2490A3#code
https://sepolia.etherscan.io/address/0x51ecB859a5b6a6aF0FbcB681FD4058E2Fe419E8A#code

```python
mint721 = EnvelopUsers721UniStorageEnumV2.at('0x1bbc7e5B329b4C79505309ABFe0c03455D2490A3')
mint1155 = EnvelopUsers1155UniStorageV2.at('0x51ecB859a5b6a6aF0FbcB681FD4058E2Fe419E8A')

mint721.mintWithURI(accounts[0],'Max URI',{'from': accounts[0], 'priority_fee': chain.priority_fee})
mint1155.mintWithURI(accounts[0],0, 1, 'Max URI',{'from': accounts[0], 'priority_fee': chain.priority_fee}
```

## Envelop User minters in Arbitrum 2023-03-24
----------Deployment artifacts-------------------
https://arbiscan.io/address/0x161fe1f0CC36212b656FC699Dc245b569a39d4AF#code
https://arbiscan.io/address/0xE692c53C2a35E88d5c58e32C58f13b4bE1AaD8f4#code


```python
accounts.load('envdeployer')
tx_params={'from': accounts[0], 'priority_fee': chain.priority_fee}
sub_reg_addr = '0xBcC0B7D29aF182f62A65f6e7d02298Cb0a0e69C2'
mint721 = EnvelopUsers721UniStorageEnumV1.at('0x161fe1f0CC36212b656FC699Dc245b569a39d4AF')
mint1155 = EnvelopUsers1155UniStorageV1.at('0xE692c53C2a35E88d5c58e32C58f13b4bE1AaD8f4')


```
## Envelop Subscription Set in Arbitrum 2023-03-24
SubRegistry: https://arbiscan.io/address/0xbcc0b7d29af182f62a65f6e7d02298cb0a0e69c2
EnvelopAgent: https://arbiscan.io/address/0x5363c1F2D9986E3eeEe914a52D8b53c79A851386

```python
accounts.load('envdeployer')
tx_params={'from': accounts[0], 'priority_fee': chain.priority_fee}

# 1. Deploy SubscriptionRegistry
#sub_reg = SubscriptionRegistry.deploy(accounts[0],tx_params, publish_source=True)
sub_reg = SubscriptionRegistry.at('0xBcC0B7D29aF182f62A65f6e7d02298Cb0a0e69C2')

# 2. Deploy SaftV2(BatchWorker)
sub_reg_addr = '0xBcC0B7D29aF182f62A65f6e7d02298Cb0a0e69C2'

# 3. Deploy EnvelopAgent
#EnvelopAgentWithRegistry.deploy(tx_params)
agent = EnvelopAgentWithRegistry.at('0x5363c1F2D9986E3eeEe914a52D8b53c79A851386')


## Init sub reg
zero_address = '0x0000000000000000000000000000000000000000' 
niftsy = interface.ERC20('0x120e49d7ab1EDc0bFBF509Fa8566ca5b5dCAAd40')
usdt = interface.ERC20('0xFd086bC7CD5C481DCC9C85ebE478A1C0b69FCbb9')

sub_reg.setAssetForPaymentState(zero_address, True, tx_params)
sub_reg.setAssetForPaymentState(niftsy, True, tx_params)
sub_reg.setAssetForPaymentState(usdt, True, tx_params)
```
